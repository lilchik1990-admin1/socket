﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Socket
{
    public class Message
    {
        public string message;
        public ClientTypeEnum type;
        private ClientTypeEnum cLIENT_1;

        public Message(string message, ClientTypeEnum cLIENT_1)
        {
            this.message = message;
            this.cLIENT_1 = cLIENT_1;
        }
    }
}
