﻿using ConsoleApp1.Browser;
using ConsoleApp1.Delegate;
using ConsoleApp1.Socket;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class ServerSocket : IWebSocketServer, SocketServerDelegate
    {
        private ServerDelegate serverDelegate;
        private SocketClientDelegate socketClientDelegate;

        public ServerSocket(ServerDelegate s1, SocketClientDelegate s2)
        {
            serverDelegate = s1;
            socketClientDelegate = s2;
        }

        public void SendMessage(Message message)
        {
            serverDelegate.Receive(message);
        }

        public void Receive(Message message)
        {
            SendMessage(message);
        }

        public void GotMessage(Message message)
        {
            socketClientDelegate.GotMessage(message);
        }
    }
}
